#!/bin/bash

#set colors
red='\033[1;31m'
gray='\033[0;37m'
green='\033[1;32m'
reset='\033[0m'

#get params or ask user
if [ $1 ];
then name=$1;
else read -p "Enter application name :" name;
fi

#Turn off outpoot
exec &> /dev/null

mkdir $name
cd $name

exec 3>&1 1>ansible.cfg
echo '#export ANSIBLE_CONFIG=$PWD/ansible.cfg'
echo ''
echo '[defaults]'
echo 'remote_user = root'
echo 'host_key_checking = false'
echo 'inventory = inventories/dev/hosts'
echo '#inventory = inventories/master/hosts'
echo 'vault_password_file = .secret'
exec 1>&3 3>&-

mkdir -p inventories/{master,dev}/{group_vars,host_vars}
mkdir -p inventories/{master,dev}/group_vars/all

touch -p inventories/{master,dev}/host_vars/host1.yml
echo -e '---\n\n...' | tee -a inventories/{master,dev}/host_vars/host1.yml

mkdir -p roles/EXAMPLE_ROLE_NAME/{templates,files,library,module_utils,lookup_plugins,tasks,handlers,vars,defaults,meta}

touch roles/EXAMPLE_ROLE_NAME/{tasks,handlers,vars,defaults,meta}/main.yml

exec 3>&1 1>roles/EXAMPLE_ROLE_NAME.yml
echo '---' 
echo '- name: EXAMPLE_ROLE_NAME'
echo '  hosts: all'
echo '  gather_facts: yes'
echo '  become: yes'
echo ''  
echo '  roles:'
echo '      - { role: EXAMPLE_ROLE_NAME, tags: SOME_TAGS }'
echo '...'
exec 1>&3 3>&-

echo '---' | tee -a roles/EXAMPLE_ROLE_NAME/{tasks,handlers,vars,defaults,meta}/main.yml
echo '- import_tasks: task1.yml' >> roles/EXAMPLE_ROLE_NAME/tasks/main.yml
echo '- import_tasks: handlers1.yml' >> roles/EXAMPLE_ROLE_NAME/handlers/main.yml
echo '...' | tee -a roles/EXAMPLE_ROLE_NAME/{tasks,handlers,vars,defaults,meta}/main.yml

echo -e '---\n\n...' >> roles/EXAMPLE_ROLE_NAME/tasks/task1.yml
echo -e '---\n\n...' >> roles/EXAMPLE_ROLE_NAME/handlers/handlers1.yml

touch inventories/{master,dev}/hosts
touch inventories/{master,dev}/group_vars/all/vars.yml

echo -e '---\nansible_port: "{{ ssh_port }}"\n...' | tee inventories/{master,dev}/group_vars/all/vars.yml


touch .secret
echo 'EAAOINY7iXT/QXsxgxtUZ08wEnx4GBIrT+LOjTn1GwFA/l9SPKVQvZJHZ9kAeS9gBwenMi5qCIjF71MvQblovEFO4J4GO+B6bJWPo0DfoTR6PMO2nlmKibBOsUvBF6CX79KYlQlXk8Y99arOhzi1hpd9Duhx/SEZEuul9bxWY+VelI4BYP+wk4kNlCXRNb0OontOdsGUcBFbxQIyM3diEqLexp5DwEUJiUvMNhw3I3K6i9lXjxDmweRno7p5RSCdfOIl67E8Ob9JAm9Q+HS2IDGbavG0r97CBH/EC9JLxIBkiU7vnZF19MJ77VONqrQc1Duf7teX+2xzEb/N2HnAqA==' > .secret

exec 3>&1 1>>RUN_ME.sh
echo '#!/bin/bash'
echo ''
echo 'export ANSIBLE_CONFIG=$PWD/ansible.cfg'
echo ''
echo 'echo "Please, inpup YML file"'
echo 'read -p "default [main.yml]: " yml_file'
echo 'ssh_port="${yml_file:=main.yml}"'
echo ''
echo 'echo "Please, inpup ssh port for remote connrction?"'
echo 'read -p "default [2022]: " ssh_port'
echo 'ssh_port="${ssh_port:=2022}"'
echo ''
echo 'echo "Debug mode need (yes/no)?"'
echo 'read -p "default [no]: " debug'
echo 'debug="${debug:=no}"'
echo ''
echo 'curent_ansible_user="$(whoami)"'
echo ''
echo '#ansible-playbook main.yml --extra-vars "ssh_port=$ssh_port curent_ansible_user=$curent_ansible_user debug=$debug"'
echo 'docker run --rm -it -v $PWD:/ansible -v ~/.ssh/id_rsa:/root/.ssh/id_rsa --workdir=/ansible minkovav/ansible:1.0 ansible-playbook main.yml --extra-vars "ssh_port=$ssh_port curent_ansible_user=$curent_ansible_user debug=$debug"'
exec 1>&3 3>&-

chmod u+x RUN_ME.sh

echo -e "This project is  ${name}" > README.txt

exec 3>&1 1>main.yml
echo '---'
echo '- import_playbook: roles/EXAMPLE_ROLE_NAME.yml'
echo '...'
exec 1>&3 3>&-

exec >/dev/tty

echo -e "${green}Done. Structure for project ${name} was created.${reset}"
