variable "name4"{
  type = string
  default = "nexus"
}


resource "yandex_compute_instance" "vm-nexus" {
  name = var.name4
  platform_id = "standard-v1"
  zone        = "ru-central1-a"
  hostname    = var.name4
  
  scheduling_policy {
    preemptible = true
  }
  
  resources {
    core_fraction = 20
    cores  = 2
    memory = 4
  }

  boot_disk {
    device_name = var.name4
    initialize_params {
      name     = var.name4
      image_id = "fd86t95gnivk955ulbq8"
      type     = "network-hdd"
      size = 30
    }
  }
  
  network_interface {
    subnet_id = "e9bk3ogcq22u9ec81dul"
    ipv6      = false
    nat       = true
  }

  metadata = {
    user-data = "${file("meta.txt")}"
  }
}

output "ip_nexus" {
  value = yandex_compute_instance.vm-nexus.network_interface[0].nat_ip_address
}

