variable "name3"{
  type = string
  default = "lb1"
}


resource "yandex_compute_instance" "vm-lb" {
  name = var.name3
  platform_id = "standard-v1"
  zone        = "ru-central1-a"
  hostname    = var.name3
  
  scheduling_policy {
    preemptible = true
  }
  
  resources {
    core_fraction = 20
    cores  = 2
    memory = 2
  }

  boot_disk {
    device_name = var.name3
    initialize_params {
      name     = var.name3
      image_id = "fd86t95gnivk955ulbq8"
      type     = "network-hdd"
      size = 15
    }
  }
  
  network_interface {
    subnet_id = "e9bk3ogcq22u9ec81dul"
    ipv6      = false
    nat       = true
  }

  metadata = {
    user-data = "${file("meta.txt")}"
  }
}

output "ip_LB1" {
  value = yandex_compute_instance.vm-lb.network_interface[0].nat_ip_address
}