variable "name"{
  type = string
  default = "app1"
}


resource "yandex_compute_instance" "vm-1" {
  name = var.name
  platform_id = "standard-v1"
  zone        = "ru-central1-a"
  hostname    = var.name
  
  scheduling_policy {
    preemptible = true
  }
  
  resources {
    core_fraction = 20
    cores  = 2
    memory = 2
  }

  boot_disk {
    device_name = var.name
    initialize_params {
      name     = var.name
      image_id = "fd86t95gnivk955ulbq8"
      type     = "network-hdd"
      size = 30
    }
  }
  
  network_interface {
    subnet_id = "e9bk3ogcq22u9ec81dul"
    ipv6      = false
    nat       = true
  }

  metadata = {
    user-data = "${file("meta.txt")}"
  }
}

output "ip_app1" {
  value = yandex_compute_instance.vm-1.network_interface[0].nat_ip_address
}

