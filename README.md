# AllInOne

:white_check_mark: Настройка инфраструктуры Terraform
:white_check_mark: Установка програм Docker, MC, Tree ...

Всё в одном что проходили мы на курсе

Структура проекта

- ANSIBLE                  - Всё что связано с проектом автоматизации
- APP                      - Приложение
- FILES                    - Файлы необходимый для проекта 
- ansible_create_proj.sh   - Скрипт генерации Ansible проекта
- create_instal_service.sh - Делаем инсталятор сервиса
- .gitlab-ci.yml           - Автоматизация на базе GitLAB

____

## Настройка воркера

### Обновление Python
```
wget https://www.python.org/ftp/python/3.9.13/Python-3.9.13.tgz
```
```
sudo apt update
sudo apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev libsqlite3-dev wget libbz2-dev
tar -xf Python-3.9.*.tgz
cd Python-3.10.*/
./configure --enable-optimizations --prefix=/usr
make -j $(nproc)
sudo make install
```

### Установка Ansible
```
pip3 install Ansible
```
___

## GitLAB
Настройки -> CI/CD -> Переменные
| Имя переменной |  Тип | Защищёная | Маскируемая |
|-------------------:|---------:|----------:|-----------------:|
|	terraform_cloud_id | Variable | [ ] | [X] |
| terraform_folder_id | Variable | [ ] | [X] |
| terraform_token | Variable | [ ] | [X] |

